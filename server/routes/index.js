import express from 'express';
import ssr from './ssr';
import opn from 'opn';

const app = express();

app.set('view engine', 'ejs');

app.use(express.static('public'));

app.use('/*', ssr);

app.listen(3000, () => {
    console.log('🚀  Listening on port 3000!');
    console.log('Opening app in default browser.');
    opn('http://localhost:3000');
});
