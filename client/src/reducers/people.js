import { PEOPLE_ACTIONS } from '../consts/action_types';
import { PEOPLE } from '../consts/default_state';

export default (state = PEOPLE, action) => {

  let imgs = {
    'Luke Skywalker': 'http://facetheforce.today/luke',
    'C-3PO': 'http://facetheforce.today/c3po',
    'R2-D2': 'http://facetheforce.today/r2d2',
    'Darth Vader': 'http://facetheforce.today/darthvader',
    'Leia Organa': 'http://facetheforce.today/leia',
    'Obi-Wan Kenobi': 'http://facetheforce.today/obiwan',
    'Anakin Skywalker': 'http://facetheforce.today/anakin',
    'Palpatine': 'http://facetheforce.today/palpatine',
    'Bail Prestor Organa': 'http://facetheforce.today/bail',
    'Han Solo': 'http://facetheforce.today/han',
    'Chewbacca': 'http://facetheforce.today/chewbacca-alt',
    'Darth Maul': 'http://facetheforce.today/darthmaul',
    'Yoda': 'http://facetheforce.today/yoda',
    'Rey': 'http://facetheforce.today/rey',
    'Finn': 'http://facetheforce.today/finn',
    'Poe Dameron': 'http://facetheforce.today/poe',
    'BB8': 'http://facetheforce.today/bb8',
    'Lando Calrissian': 'http://facetheforce.today/lando',
    'Padmé Amidala': 'http://facetheforce.today/amidala',
    'Mace Windu': 'http://facetheforce.today/macewindu',
    'Qui-Gon Jinn': 'http://facetheforce.today/quigon',
    'Jabba Desilijic Tiure': 'http://facetheforce.today/jaba',
    'Boba Fett': 'http://facetheforce.today/boba',
    'Greedo': 'http://facetheforce.today/greedo',
    'Mon Mothma': 'http://facetheforce.today/mon',
  };

  switch (action.type) {
    case PEOPLE_ACTIONS.PEOPLE_VIEW:
      const person = (action.list === 'saved')
          ? state.saved[action.name.toUpperCase()]
          : state.people[action.name.toUpperCase()];
      return { ...state, personView: person };

    case PEOPLE_ACTIONS.PEOPLE_REMOVE: {
      const list = (action.list === 'saved') ? state.saved : state.people;
      const nextSaved = {...list};
      delete nextSaved[action.name.toUpperCase()];
      let result = {...state};
      result[action.list] = nextSaved;
      return result;
    }

    case PEOPLE_ACTIONS.PEOPLE_ADD: {
      const nextPeople = { ...state.people };
      const personToAdd = action.person;
      if (!personToAdd.img) {
        personToAdd.img = imgs[personToAdd.name];
      }
      nextPeople[personToAdd.name.toUpperCase()] = personToAdd;
      return { ...state, people: nextPeople };
    }

    case PEOPLE_ACTIONS.PEOPLE_SAVE: {
      const nextPeople = { ...state.saved };
      const personToAdd = action.person;
      nextPeople[personToAdd.name.toUpperCase()] = personToAdd;
      return { ...state, saved: nextPeople };
    }

    case PEOPLE_ACTIONS.FILTER_GENDER: {
      return { ...state, gender: action.gender };
    }

    default:
      return state;
  }
};
