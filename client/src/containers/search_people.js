import { connect } from 'react-redux';
import {
  showPersonInfo,
  addPerson,
  removePerson,
  savePerson
} from '../actions/people_actions';
import SearchPersonView from '../components/search_people';

const mapStateToProps = state => ({
  people: state.people.people,
});

const mapDispatchToProps = dispatch => ({
  showPersonInfo: (name, list) => {
    dispatch(showPersonInfo(name, list));
  },
  addPerson: (person) => {
    dispatch(addPerson(person));
  },
  savePerson: (person) => {
    dispatch(savePerson(person));
    dispatch(removePerson(person.name, 'people'));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchPersonView);
