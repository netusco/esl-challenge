import { connect } from 'react-redux';
import PeopleSaved from '../components/people_saved';
import {
  showPersonInfo,
  removePerson,
  filterGender,
  addPerson,
} from '../actions/people_actions';

function mapStateToProps(state) {
  return {
    saved: state.people.saved,
    gender: state.people.gender,
  };
}

const mapDispatchToProps = dispatch => ({
  showPersonInfo: (name, list) => {
    dispatch(showPersonInfo(name, list));
  },
  removePerson: (person) => {
    dispatch(removePerson(person.name, 'saved'));
    dispatch(addPerson(person));
  },
  filterGender: (gender) => {
    dispatch(filterGender(gender));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(PeopleSaved);