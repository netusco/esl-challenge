import { connect } from 'react-redux';
import PersonView from '../components/person_view';

function mapStateToProps(state) {
    return {
        person: state.people.personView,
    };
}

export default connect(mapStateToProps)(PersonView);