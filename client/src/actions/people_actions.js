import { PEOPLE_ACTIONS } from '../consts/action_types';

export const showPersonInfo = (name, list = 'people') => ({
  type: PEOPLE_ACTIONS.PEOPLE_VIEW,
  name,
  list,
});

export const addPerson = person => ({
  type: PEOPLE_ACTIONS.PEOPLE_ADD,
  person,
})

export const savePerson = person => ({
  type: PEOPLE_ACTIONS.PEOPLE_SAVE,
  person,
});

export const removePerson = (name, list = 'people') => ({
  type: PEOPLE_ACTIONS.PEOPLE_REMOVE,
  name,
  list,
});

export const filterGender = (gender) => ({
    type: PEOPLE_ACTIONS.FILTER_GENDER,
    gender
})
