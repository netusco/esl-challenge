import React from 'react';

const Header = () => (
  <div style={{ marginTop: 20 }} className="header">
    <div className="header-title">
    </div>
    <h3>Web test</h3>
    <h4>Play with SWAPI</h4>
  </div>
);

export default Header;
