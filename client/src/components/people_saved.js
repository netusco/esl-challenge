import React from 'react';

const PeopleSaved = ({ saved, gender, showPersonInfo, removePerson, filterGender }) => {

    const renderPeople = () => {
        if (Object.keys(saved).length <= 0) {
            return ( <p className="card-text">You didn't save any people yet.</p> )
        }

        // filter results by selected gender
        let peopleByGender = Object.keys(saved).filter((key) => (gender === 'All' || saved[key].gender === gender));
        return peopleByGender.map(key => {
            return (
                <div className="card text-center">
                    <img className="card-image" src={saved[key].img} alt={saved[key].name} />
                    <h6 className="card-title">{saved[key].name}</h6>
                    <div className="btn-grup">
                        <button className="btn btn-primary" onClick={() => showPersonInfo(saved[key].name, 'saved')}>Show Info</button>
                        <button className="btn btn-primary" onClick={() => removePerson(saved[key])}>Remove</button>
                    </div>
                </div>
            )
        });
    }

    const renderGenderFilter = () => {
        let diffGenders = false;
        let genders = ['All'];

        // filter is only displayed if exists more than 1 person with different gender saved
        if (Object.keys(saved).length > 1) {
            let lastGender;

            Object.keys(saved).forEach(key => {
               if (!lastGender) {
                   lastGender = saved[key].gender;
                   genders.push(saved[key].gender);
               } else if (lastGender !== saved[key].gender) {
                   diffGenders = true;
                   genders.push(saved[key].gender);
               }
            });
        }

        // return all different existent genders within saved people
        return (!diffGenders) ? '' : (
            <div className="btn-group gender-filter mb-3">
                { genders.map((gender) => {
                    return <button onClick={() => filterGender(gender)} className="btn btn-secondary">{ gender }</button>
                }) }
            </div>
        )

    }

    return (
        <div className="saved_people card specific_cards">
            <div className="card-body">
                <h5 className="card-title">Saved people</h5>
                { renderGenderFilter() }
                { renderPeople() }
            </div>
        </div>
    );
};


export default PeopleSaved;