import React from 'react';

const PersonView = ({ person }) => {

    return (!person)
      ? (
            <div className="person_view card specific_cards">
                <div className="card-body">
                    <h5 className="card-title">Information about...</h5>
                    <p className="card-text">Choose someone to get more information about!</p>
                </div>
            </div>
        )
      : (
            <div className="person_view card specific_cards">
                <div className="card-body">
                    <h5 className="card-title">Information about...</h5>
                    <div className="card">
                        <img className="card-image" src={ person.img }  alt={ person.name } />
                        <h5 className="card-text"> { person.name } </h5>
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item"><strong>Height</strong>: { person.height }</li>
                            <li className="list-group-item"><strong>Mass</strong>: { person.mass }</li>
                            <li className="list-group-item"><strong>Hair color</strong>: { person.hair_color }</li>
                            <li className="list-group-item"><strong>Skin color</strong>: { person.skin_color }</li>
                            <li className="list-group-item"><strong>Eye color</strong>: { person.eye_color }</li>
                            <li className="list-group-item"><strong>Birth year</strong>: { person.birth_year }</li>
                            <li className="list-group-item"><strong>Gender</strong>: { person.gender }</li>
                        </ul>
                    </div>
                </div>
            </div>
    );
};


export default PersonView;
