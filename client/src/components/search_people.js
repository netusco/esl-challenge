import React, { Component } from 'react';
import axios from 'axios';


class SearchPersonView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      displayed: [],
    };

    axios
        .get('https://swapi.co/api/people/')
        .then((response) => {
            return response.data.results.map((person) => {
                this.props.addPerson(person);
            });
        })
        .catch((err) => {
            console.log('Error fetching source data:-S', err);
        });

    this.onInputChange = this.onInputChange.bind(this);
  }

  setResults(value) {
      let matchedResults = (value == '')
          ? []
          : Object.values(this.props.people).filter(person => person.name.toUpperCase().startsWith(value.toUpperCase()));

      this.setState({
          searchTerm: value,
          displayed: matchedResults,
      });
  }

  onInputChange(event) {
    this.setResults(event.target.value);
  }

  renderList() {
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Gender</th>
                <th scope="col">Birth year</th>
                <th scope="col">Eye color</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
                { this.renderPeople() }
            </tbody>
        </table>
    )
  }

  renderPeople() {
    return this.state.displayed.map((person) => {
      return (
          <tr>
            <th scope="row">{ person.name }</th>
            <td>{ person.gender }</td>
            <td>{ person.birth_year }</td>
            <td>{ person.eye_color }</td>
            <td>
                <div className="btn-grup">
                    <button className="btn btn-primary" onClick={() => this.props.showPersonInfo(person.name, 'people')}>Show Info</button>
                    <button className="btn btn-primary" onClick={() => {
                        this.props.savePerson(person);
                        setTimeout(() => this.setResults(this.state.searchTerm), 50);
                    }}>Save</button>
                </div>
            </td>
          </tr>
      )
    });
  }

  renderSearchBox() {
    return (
      <form className="form-inline" onSubmit={(e) => { e.preventDefault(); }}>
        <div className="form-group mb-3">
          <label className="sr-only">Name</label>
          <input className="form-control" type="text" onChange={ this.onInputChange } />
        </div>
      </form>
    )
  }


  render() {
    return (
      <div className="people_list card specific_cards">
          <div className="card-body">
            <h5 className="card-title">Search people</h5>
              { this. renderSearchBox() }
              { this.renderList() }
          </div>
      </div>
    );
  }
}

export default SearchPersonView;
