import React from 'react';
import SearchPeople from '../containers/search_people';
import PersonView from '../containers/people_view';
import PeopleSaved from '../containers/people_saved';

const Home = () => (
  <div className="home">
      <SearchPeople />
      <PersonView />
      <PeopleSaved />
  </div>
);

export default Home;
